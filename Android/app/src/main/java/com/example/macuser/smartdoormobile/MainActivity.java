package com.example.macuser.smartdoormobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.macuser.smartdoormobile.bluetooth.BtObserver;
import com.example.macuser.smartdoormobile.bluetooth.ConnectionManager;
import com.example.macuser.smartdoormobile.utilities.Utilities;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BtObserver {

    public static boolean LOGGED = false;
    private Login login;
    private Controller controller;
    private Home home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SETTING THE VIEW
        setContentView(R.layout.activity_main);
        //SETTING THE TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //SETTING THE DRAWER
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //SETTING NAVIGATION VIEW
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        this.login = new Login();
        this.controller = Controller.getInstance();
        this.home = new Home();
        ConnectionManager.getInstance().attacheObserver(this);


        //SETTING THE DEFAULT NAV VIEW
        navigationView.setCheckedItem(R.id.nav_home);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mainFrame, this.home);
        this.getSupportActionBar().hide();
        ft.commit();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, this.home);
            this.getSupportActionBar().hide();
            ft.commit();
        } else if (id == R.id.nav_control) {
            if (checkLogged()) {
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.mainFrame, this.controller);
                this.getSupportActionBar().show();
                ft.commit();
            }
        } else if (id == R.id.nav_login) {
            if (checkConnected()) {
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.mainFrame, this.login);
                this.getSupportActionBar().show();
                ft.commit();
            }
        } else if (id == R.id.nav_bluetooth) {
            Intent bluetoothIntent = new Intent(this, BluetoothActivity.class);
            bluetoothIntent.putExtra("MESSAGE_FROM_CREATOR", "i'm your father");
            startActivity(bluetoothIntent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean checkLogged() {
        if (!this.checkConnected()) {
            return false;
        }
        if (!LOGGED) {
            Toast.makeText(this, "Login first", Toast.LENGTH_LONG).show();

            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, this.login);
            this.getSupportActionBar().show();
            ft.commit();
            return false;
        } else {
            return true;
        }
    }

    private boolean checkConnected() {
        if (!BluetoothActivity.CONNECTED) {
            Toast.makeText(this, "Connect first", Toast.LENGTH_LONG).show();

            Intent bluetoothIntent = new Intent(this, BluetoothActivity.class);
            bluetoothIntent.putExtra("MESSAGE_FROM_CREATOR", "i'm your father");
            startActivity(bluetoothIntent);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void udpateTemp(String temp) {
        this.controller.updateVal(temp);
    }

    @Override
    public void serverLoginCheck(boolean login) {
        if (login) {
            LOGGED = true;
            runOnUiThread(() -> {
                Toast.makeText(this, "Credentials Correct", Toast.LENGTH_LONG).show();
                Utilities.notification(this, "Credentials", "Correct");
            });
        } else {
            runOnUiThread(() -> {
                Toast.makeText(this, "Credentials Incorrect", Toast.LENGTH_LONG).show();
                Utilities.notification(this, "Credentials", "Incorrect");
            });
        }
    }

    @Override
    public void mobileNear() {
        runOnUiThread(() -> {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, this.login);
            this.getSupportActionBar().show();
            ft.commitAllowingStateLoss();
        });
    }

    @Override
    public void closed() {
        LOGGED = false;
        runOnUiThread(() -> {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, this.login);
            this.getSupportActionBar().show();
            ft.commitAllowingStateLoss();
        });
    }

    @Override
    public void inside(boolean b) {
        if (b) {
            runOnUiThread(() -> {
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.mainFrame, this.controller);
                this.getSupportActionBar().show();
                ft.commitAllowingStateLoss();
            });
        } else {
            LOGGED = false;
            this.mobileNear();
        }
    }
}
