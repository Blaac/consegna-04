package com.example.macuser.smartdoormobile.bluetooth;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.macuser.smartdoormobile.BluetoothActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by macuser on 29/01/18.
 */

public class ConnectionManager extends Thread {
    private Set<BtObserver> observer;

    private BluetoothSocket btSocket;
    private BufferedReader bufferReader;
    private BufferedWriter bufferedWriter;
    private boolean stop;
    private static ConnectionManager instance = null;

    private ConnectionManager(){
        stop = true;
        this.observer = new HashSet<>();
    }

    public static ConnectionManager getInstance(){
        if(instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

    public void setChannel(BluetoothSocket socket){
        btSocket = socket;
        try {
            this.bufferReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch(IOException e) {
            /*...*/
        }
        stop = false;
    }

    public void run() {
        byte[] buffer = new byte[1024];
        int nBytes = 0;
        while(!stop) {
            try {
                this.read(this.bufferReader.readLine());
            /* Manage here the data stored in buffer */
            } catch (IOException e) {
                stop = true;
            }
        }
    }

    public boolean write(String msg){
        if(bufferedWriter == null) {
            return false;
        }
        try{
            bufferedWriter.write(msg);
            bufferedWriter.flush();
        } catch(IOException e){
            return false;
        }
        return true;
    }

    private void read(String data) {
        Log.v(this.getClass().toString(), data);
        if (data.startsWith("7")) {
            this.observer.forEach(t -> t.udpateTemp(data.substring(1)));
        } else if (data.startsWith("2")) {
            if (data.substring(1).equals("false")) {
                this.observer.forEach(t -> t.serverLoginCheck(false));
            } else {
                this.observer.forEach(t -> t.serverLoginCheck(true));
            }
        } else if (data.startsWith("3")) {
            this.observer.forEach(t -> t.mobileNear());
        } else if (data.startsWith("8")) {
            this.observer.forEach(t -> t.closed());
        }else if (data.startsWith("6")) {
            if (data.substring(1).equals("false")) {
                this.observer.forEach(t -> t.inside(false));
            } else {
                this.observer.forEach(t -> t.inside(true));
            }
        }
    }

    public void cancel() {
        try{
            btSocket.close();
            BluetoothActivity.CONNECTED = false;
        } catch(IOException e){
            /*...*/
        }
    }

    public void attacheObserver(BtObserver observer) {
        this.observer.add(observer);
    }
}
