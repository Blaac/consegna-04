package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.macuser.smartdoormobile.bluetooth.BtObserver;
import com.example.macuser.smartdoormobile.bluetooth.ConnectionManager;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Controller.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Controller#newInstance} factory method to
 * create an instance of this fragment.
 */
public final  class Controller extends android.support.v4.app.Fragment {
    private View view;
    private Context context;
    private Bundle saved;
    private Activity activity;

    private SeekBar seek;
    private TextView text;
    private TextView labelTemp;
    private Button buttonClose;

    private static Controller instance = null;

    public static Controller getInstance() {
        if(instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public  Controller() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = container.getContext();
        this.saved = savedInstanceState;
        this.activity = this.getActivity();
        this.view =  inflater.inflate(R.layout.fragment_controller, container, false);

        this.buttonClose = (Button)this.view.findViewById(R.id.button_close);
        this.seek = (SeekBar)this.view.findViewById(R.id.seekBar);
        this.text = (TextView)this.view.findViewById(R.id.update_light_val);
        final ImageView bulb = (ImageView)this.view.findViewById(R.id.bulb);
        this.labelTemp = (TextView)this.view.findViewById(R.id.label_temp_sensor);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                text.setText("" + i);
                seek.setAlpha((float)i/100 + 0.2f);
                bulb.getDrawable().setTint(Color.rgb(map(i, 0, 100, 0, 255) , map(i, 0, 100, 0, 245), 0));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ConnectionManager.getInstance().write("7"+seekBar.getProgress());

            }
        });

        this.buttonClose.setOnClickListener(e -> {

            MainActivity.LOGGED = false;
            ConnectionManager.getInstance().write("8");
            ((MainActivity)this.activity).closed();

        });



        return this.view;
    }

    int map(int x, int in_min, int in_max, int out_min, int out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    public void updateVal(String msg) {
        Log.v("tag", msg);
        this.activity.runOnUiThread(() ->{
            labelTemp.setText(msg + "°C");
        });
    }

    public void udpateTemp(String temp) {
        this.updateVal(temp);
    }
}

