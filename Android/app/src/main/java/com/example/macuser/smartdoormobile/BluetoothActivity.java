package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.macuser.smartdoormobile.bluetooth.BluetoothAdapterz;
import com.example.macuser.smartdoormobile.bluetooth.ClientConnectionTask;
import com.example.macuser.smartdoormobile.bluetooth.ConnectionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BluetoothActivity extends AppCompatActivity {
    public static boolean CONNECTED = false;
    public static boolean NEAR = false;
    //MY DEVICE
    private BluetoothDevice device;
    //CREO ADAPTER E ENABLE
    private BluetoothAdapter btAdapter;
    private final int ENABLE_BT_REQ = 1;
    //INIT DELLA LISTA DI PAIRED
    private List<BluetoothDevice> pairedList;
    private ListView pairedListView;
    private BluetoothAdapterz adapterPaired;
    private List<BluetoothDevice> allDeviceList;
    //BUTTON
    private Button refreshButton;
    private Button disconnectButton;
    //UID
    private static final String APP_UUID = "00001101-0000-1000-8000-00805F9B34FB";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SET VIEW
        setContentView(R.layout.activity_bluetooth);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //INIT BLUETOOTH AND LIST
        this.btAdapter = BluetoothAdapter.getDefaultAdapter();
        this.pairedListView = (ListView)findViewById(R.id.list_view_paired);
        this.pairedList = new ArrayList<>();
        this.adapterPaired = new BluetoothAdapterz(this, pairedList);
        this.pairedListView.setAdapter(this.adapterPaired);

        //BUTTON
        this.refreshButton = (Button)this.findViewById(R.id.button_bluetooth_refresh);
        this.disconnectButton = (Button)this.findViewById(R.id.button_bluetooth_disconnect);
        if (CONNECTED) {
            this.disconnectButton.setEnabled(true);
        } else {
            this.disconnectButton.setEnabled(false);
        }

        this.disconnectButton.setOnClickListener(e -> {
            ConnectionManager.getInstance().cancel();
            this.disconnectButton.setEnabled(false);
            Toast.makeText(this, R.string.disconnected_bluetooth, Toast.LENGTH_LONG).show();

        });
        this.refreshButton.setOnClickListener(e -> {
            this.adapterPaired.notifyDataSetChanged();
        });

        //SET LISTENER FOR LISTVIEW
        pairedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                BluetoothActivity.this.device = (BluetoothDevice)parent.getItemAtPosition(position);
                startConnection();
            }
        });

        //INIT BLUETOOTH AND GET PAIRED DEVICES
        if(btAdapter == null){
            Log.e("MyApp","BT is not available on this device"); finish();
        }

        if (!btAdapter.isEnabled()) {
            startActivityForResult( new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                ENABLE_BT_REQ);
        } else {
            this.pairedList.addAll(BluetoothAdapter.getDefaultAdapter().getBondedDevices().stream().collect(Collectors.toList()));
            this.adapterPaired.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int reqID, int res, Intent data){
        if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_OK){ //BT enabled
            Toast.makeText(this, R.string.accept_bluetooth, Toast.LENGTH_LONG).show();
            this.pairedList.addAll(BluetoothAdapter.getDefaultAdapter().getBondedDevices().stream().collect(Collectors.toList()));
            this.adapterPaired.notifyDataSetChanged();
        }
        if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_CANCELED){ //BT enabling process aborted
            Toast.makeText(this, R.string.deny_bluetooth, Toast.LENGTH_LONG).show();
            Intent goToMainActivity = new Intent(this, MainActivity.class);
            startActivity(goToMainActivity);
        }
    }

    private void startConnection() {
        UUID uuid = UUID.fromString(APP_UUID);
        new ClientConnectionTask(this.device , uuid, this).execute();
    }
}
