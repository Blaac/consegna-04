package com.example.macuser.smartdoormobile.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.macuser.smartdoormobile.BluetoothActivity;
import com.example.macuser.smartdoormobile.MainActivity;
import com.example.macuser.smartdoormobile.R;

import java.io.IOException;
import java.util.UUID;

/**
 * Create the connection.
 */

public class ClientConnectionTask extends AsyncTask<Void, String, Void> {
    private BluetoothSocket btSocket = null;
    private Context context;

    public ClientConnectionTask(BluetoothDevice server , UUID uuid, Context context){
        this.context = context;
        try {
            btSocket = server.createRfcommSocketToServiceRecord(uuid);
            Log.v("s", "socket Created");
        } catch(IOException e){
            /* ... */
        }
    }

    @Override
    protected Void doInBackground(Void... params){
        try{
            btSocket.connect();
        } catch (IOException connectException) {
            try{
                btSocket.close();
                publishProgress("NO_CONNECTION");
            } catch(IOException closeException) {
                publishProgress("CONN_PROBLEM");
            }
            return null;
        }
        ConnectionManager cm = ConnectionManager.getInstance();
        cm.setChannel(btSocket);
        cm.start();
        publishProgress("CONNECTED");
        cm.write("0:connected");
        BluetoothActivity.CONNECTED = true;
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if (values[0].equals("NO_CONNECTION")) {
            Toast.makeText(this.context, R.string.disconnected_bluetooth, Toast.LENGTH_LONG).show();
        } else if (values[0].equals("CONN_PROBLEM")) {
            Toast.makeText(this.context, R.string.problem_bluetooth, Toast.LENGTH_LONG).show();
        } else if (values[0].equals("CONNECTED")) {
            Toast.makeText(this.context, R.string.connected_bluetooth, Toast.LENGTH_LONG).show();
            ((Button)((Activity) context).findViewById(R.id.button_bluetooth_disconnect)).setEnabled(true);
            Intent mainIntent = new Intent(this.context, MainActivity.class);
            mainIntent.putExtra("MESSAGE_FROM_CREATOR", "i'm your father");
            this.context.startActivity(mainIntent);
        }
    }

}
