package com.example.macuser.smartdoormobile.bluetooth;

/**
 * Created by macuser on 01/02/18.
 */

public interface BtObserver {
    void udpateTemp(String temp);
    void serverLoginCheck(boolean login);
    void mobileNear();
    void closed();
    void inside(boolean b);
}
