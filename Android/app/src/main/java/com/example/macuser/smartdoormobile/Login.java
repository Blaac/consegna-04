package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.macuser.smartdoormobile.bluetooth.BtObserver;
import com.example.macuser.smartdoormobile.bluetooth.ConnectionManager;
import com.example.macuser.smartdoormobile.utilities.SecureHash;
import com.example.macuser.smartdoormobile.utilities.Utilities;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Login.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Login#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Login extends android.support.v4.app.Fragment {
    private View view;
    private Context context;
    private Bundle saved;
    private Activity activity;

    private TextView username;
    private TextView password;

    public Login() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.context = container.getContext();
        this.saved = savedInstanceState;
        this.activity = this.getActivity();
        this.view = inflater.inflate(R.layout.fragment_login, container, false);
        this.password = (TextView)this.view.findViewById(R.id.text_password);
        this.username = (TextView)this.view.findViewById(R.id.text_username);

        Button button = (Button) this.view.findViewById(R.id.button_send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login.this.sendCredentials();
            }
        });


        return this.view;
    }

    private void sendCredentials() {
        Thread hasher = new Thread(() -> {

            if (!this.password.getText().toString().equals("")) {
                try {
                    String hash = SecureHash.generateStorngPasswordHash(this.password.getText().toString());
                    hash = this.password.getText().toString();
                    String user = this.username.getText().toString();
                    String encript = "1" + user + "," + hash ;
                    ConnectionManager.getInstance().write(encript);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
            }
        });
        hasher.start();
    }


}
