package com.jradar.textui;

import java.util.List;

/**
 * The {@link ConsoleView}.
 *
 */
public class ConsoleView implements UI {
    //private final Controller controller;

    @Override
    public void updateViewReceived(final String inputLine) {
        System.out.println(inputLine);
    }

    @Override
    public void welcome() {
        System.out.println("\n############## Welcome to SmartDoor #############");
        System.out.println("\n   (1) Select port! \n   (2) Go! \n   (3) [exit] to Exit App\n");
    }

    @Override
    public void printPorts(final List<String> ports) {
        System.out.println("(PORTS) > SELECT THE PORT -> [port:<number>]");
        ports.stream().forEach(t -> System.out.println(t));
    }

}
