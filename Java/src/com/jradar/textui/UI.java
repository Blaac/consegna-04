package com.jradar.textui;

import java.util.List;

/**
 * UI.
 *
 */
public interface UI {

    /**
     * Update.
     *
     * @param inputLine
     *            the inputline
     */
    void updateViewReceived(String inputLine);

    /**
     * Welcome.
     */
    void welcome();

    /**
     * Print ports.
     *
     * @param ports
     *            the {@link List} of ports
     */
    void printPorts(List<String> ports);

}