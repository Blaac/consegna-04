package com.jradar.model.devices;

/**
 * Button.
 *
 */
public interface Button extends Device {
    /**
     * If pressed.
     *
     * @return true if pressed
     */
    boolean isPressed();

}
