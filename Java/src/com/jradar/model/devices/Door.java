/**
 * The Radar.java class.
 */
package com.jradar.model.devices;

import com.jradar.model.observable.Observer;

/**
 * The Radar class.
 */
/**
 * The Radar class.
 */
/**
 * The Radar class.
 */
/**
 * The Radar class.
 */
public interface Door extends Observer, Device {
    /**
     * If the door is open.
     *
     * @return <code>true</code> if the door is open
     */
    boolean isOpen();
}
