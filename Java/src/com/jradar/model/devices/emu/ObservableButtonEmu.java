package com.jradar.model.devices.emu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.jradar.controller.event.EventMsg;
import com.jradar.controller.event.EventType;
import com.jradar.model.devices.Button;
import com.jradar.model.observable.Observable;

/**
 * Button emu.
 *
 *
 */
public class ObservableButtonEmu extends Observable implements Button {

    @SuppressWarnings("unused")
    private final int pinNum; //NOPMD
    private boolean isPressed; //NOPMD
    private ButtonFrame buttonFrame; //NOPMD
    private final EventType eventType;

    /**
     * Constructor.
     *
     * @param pinNum
     *            the pin
     * @param eventType
     *            the {@link EventType}
     */
    public ObservableButtonEmu(final int pinNum, final EventType eventType) {
        System.out.println("[BUTTON " + eventType + "] installed on pin " + pinNum);
        this.pinNum = pinNum;
        this.eventType = eventType;
        try {
            this.buttonFrame = new ButtonFrame(pinNum);
            this.buttonFrame.setVisible(true);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized boolean isPressed() {
        return this.isPressed;
    }

    private void setPressed(final boolean state) {
        this.isPressed = state;
        if (this.isPressed) {
            this.notifyObservers(new EventMsg(this.eventType));
        } else {
            this.notifyObservers(new EventMsg(EventType.BUTTON_RELEASED));
        }
    }

    class ButtonFrame extends JFrame implements MouseListener {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        ButtonFrame(final int pin) {
            super("Button Emu");
            //CHECKSTYLE:OFF
            this.setSize(200, 50);
            //CHECKSTYLE:ON
            final JButton button = new JButton("Button on pin: " + pin);
            button.addMouseListener(this);
            this.getContentPane().add(button);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(final WindowEvent ev) {
                    System.exit(-1);
                }
            });
        }

        @Override
        public void mousePressed(final MouseEvent e) {
            ObservableButtonEmu.this.setPressed(true);
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            ObservableButtonEmu.this.setPressed(false);
        }

        @Override
        public void mouseEntered(final MouseEvent e) {
        }

        @Override
        public void mouseExited(final MouseEvent e) {
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
        }
    }

}
