package com.jradar.model.devices.emu;

import com.jradar.model.devices.Light;

/**
 * Led emu.
 *
 */
public class Led implements Light {
    @SuppressWarnings("unused")
    private final int pinNum; //NOPMD
    private final String id;

    /**
     * The constructor.
     *
     * @param pinNum
     *            the pin
     * @param id
     *            the id
     */
    public Led(final int pinNum, final String id) {
        this.pinNum = pinNum;
        this.id = id;
        try {
            System.out.println("[LED " + id + "] installed on pin " + pinNum);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor.
     *
     * @param pinNum
     *            the pin
     */
    public Led(final int pinNum) {
        this(pinNum, "green");
    }

    @Override
    public synchronized boolean switchOn() throws Exception {
        System.out.println("[LED " + this.id + "] ON");
        return true;
    }

    @Override
    public synchronized boolean switchOff() throws Exception {
        System.out.println("[LED " + this.id + "] OFF");
        return true;
    }

}
