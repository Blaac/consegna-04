package com.jradar.model.devices.emu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.jradar.model.devices.Button;

/**
 * Button Emu.
 *
 *
 */
public class ButtonEmu implements Button {

    @SuppressWarnings("unused")
    private final int pinNum; //NOPMD
    private boolean isPressed; //NOPMD
    private ButtonFrame buttonFrame; //NOPMD

    /**
     * Cosntructor.
     *
     * @param pinNum
     *            the pin
     */
    public ButtonEmu(final int pinNum) {
        this.pinNum = pinNum;
        try {
            this.buttonFrame = new ButtonFrame(pinNum);
            this.buttonFrame.setVisible(true);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized boolean isPressed() {
        return this.isPressed;
    }

    private void setPressed(final boolean state) {
        this.isPressed = state;
    }

    class ButtonFrame extends JFrame implements MouseListener {
        /**
        *
        */
        private static final long serialVersionUID = 1L;

        /**
         * The constructor.
         *
         * @param pin
         *            the pin
         */
        ButtonFrame(final int pin) {
            super("Button Emu");
            //CHECKSTYLE:OFF
            this.setSize(200, 50);
            //CHECKSTYLE:ON
            final JButton button = new JButton("Button on pin: " + pin);
            button.addMouseListener(this);
            this.getContentPane().add(button);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(final WindowEvent ev) {
                    System.exit(-1);
                }
            });
        }

        @Override
        public void mousePressed(final MouseEvent e) {
            ButtonEmu.this.setPressed(true);
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            ButtonEmu.this.setPressed(false);
        }

        @Override
        public void mouseEntered(final MouseEvent e) {
        }

        @Override
        public void mouseExited(final MouseEvent e) {
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
        }
    }

}
