/**
 * The RadarImpl.java class.
 */
package com.jradar.model.devices.door;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONException;
import org.json.JSONObject;

import com.jradar.controller.event.Event;
import com.jradar.controller.event.EventMsg;
import com.jradar.controller.event.EventType;
import com.jradar.model.agents.SerialMonitorAgent;
import com.jradar.model.devices.Door;
import com.jradar.model.devices.Light;
import com.jradar.model.observable.Observable;

/**
 * The RadarImpl class.
 */
public class DoorImpl implements Door {

    private final static String FAILED_LOG = "Access denied";
    private final static String ENTERED = "Working session started";
    private final static String NOT_ENTERED = "User not detected";
    private final String driver;
    private final String dbUri;
    private final String userName;
    private final String password;
    private String utente = "user";  // initial value -> to be changed during log-in phase
    private final SerialMonitorAgent serialAgent;
    private final boolean isOpen;
    private final Connection conn;
    private final Light ledInside;
    private final Light ledFailedAccess;

    /**
     * Constructor of the Class RadarImpl.
     *
     * @param serialAgent
     *            the {@link SerialMonitorAgent}
     * @param led1
     *            led
     * @param led2
     *            led2
     *
     */
    public DoorImpl(final SerialMonitorAgent serialAgent, final Light led1, final Light led2) {
        this.ledInside = led1;
        this.ledFailedAccess = led2;
        this.driver = "com.mysql.jdbc.Driver";
        this.dbUri = "jdbc:mysql://localhost:3306/SmartDoor";
        this.userName = "root";
        this.password = "root";
        this.conn = this.startConnection();
        this.serialAgent = serialAgent;
        this.serialAgent.addObserver(this);
        this.serialAgent.getClass();
        this.isOpen = false;
    }

    @Override
    public boolean isOpen() {
        return this.isOpen;
    }

    // Mando il messaggio "sopra" all'observer
    @Override
    public boolean notifyObserver(final Event ev) {
        if (ev.getEventType().equals(EventType.ARDUINO_SERIAL_MSG)) {
            if (ev.getMessage().get().startsWith(EventType.LOGIN_MSG.getMessage())) {
            	try {
            		
            		final String[] message = ev.getMessage().get().substring(1).split(",");
            		final String username = message[0];
            		final String password = message[1];
            		System.out.println("USER-" + username);
            		System.out.println("PASS-" + password);
            		// check credenziali
            		 try {
                         this.withdrawCredentials(username, password);
                     } catch (final Exception e) {
                         e.printStackTrace();
                     } 
            	} catch(Exception e) {
            		e.printStackTrace();
            	}
               
            } else if (ev.getMessage().get().startsWith(EventType.LOG_MSG.getMessage())) {
            	
            	try{
            		final String[] message = ev.getMessage().get().substring(1).split(",");
            		final String temp = message[0];
                    final String ledValue = message[1];
                    this.logOnJSON(temp, ledValue);
            	} catch(Exception e) {

            		e.printStackTrace();
            	}
            	
                
            }
            else if (ev.getMessage().get().startsWith(EventType.PRESENCE_DETECTED.getMessage())) {
                try {
                    this.logOnSQL(this.utente, ENTERED);
                } catch (final SQLException e1) {
                    e1.printStackTrace();
                }
                try {
                    this.ledInside.switchOn();
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            } else if (ev.getMessage().get().startsWith(EventType.PRESENCE_NOT_DETECTED.getMessage())) {
                try {
                    this.logOnSQL(this.utente, NOT_ENTERED);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return true;
    }

    private Connection startConnection() {
        Connection connection = null;
        try {
            Class.forName(this.driver);
            connection = DriverManager.getConnection(this.dbUri, this.userName, this.password);
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Errore CNF " + e.getMessage());
        } catch (final SQLException e) {
            e.printStackTrace();
            System.out.println("Errore SQL " + e.getMessage());
        }
        return connection;
    }

    private void withdrawCredentials(final String user, final String pass) throws Exception {
        Statement statement = this.conn.createStatement();
        try {
            ResultSet rs;
            final PreparedStatement prepstmt = this.conn
                    .prepareStatement("SELECT * FROM utenti where username = ? AND Password = ?");
            prepstmt.setString(1, user);
            prepstmt.setString(2, pass);
            rs = prepstmt.executeQuery();
            if (rs.next()) {            // utente presente nel db
                this.utente = user;
                this.serialAgent.sendData(EventType.ACCESS_GRANTED.getMessage());
                prepstmt.close();
//                this.logOnSQL(user, ENTERED);
                System.out.println("GRANTED");
            } else {
                prepstmt.close();
                this.logOnSQL(user, FAILED_LOG);
                this.utente = "user";
                System.out.println("DENIED");
                this.flickerLed(this.ledFailedAccess);
                this.serialAgent.sendData(EventType.ACCESS_DENIED.getMessage());
            }
            prepstmt.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    private void flickerLed(final Light led) throws Exception {
        led.switchOn();
        Thread.sleep(100);
        led.switchOff();
    }
    
    private void logOnJSON(final String degrees, final String ledIntensity) {
        JSONObject JSONFile = new JSONObject();
        try {
            JSONFile.put("lvalue", ledIntensity);
            JSONFile.put("temp", degrees);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        String home = System.getProperty("user.home");
        String separ = System.getProperty("file.separator");
        String path = "C:\\MAMP\\htdocs\\Site";
        String javaPath = path.replace("\\", "/");
        File f = new File(javaPath+separ+"sens.json");
        try (FileWriter fw = new FileWriter(f)) {
            fw.write(JSONFile.toString());
//            System.out.println(f.getAbsolutePath());
            fw.flush();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void logOnSQL(final String username, final String logmessage) throws SQLException {
        final ResultSet rs;
        final PreparedStatement preparedStatement = this.conn
                                                        .prepareStatement("INSERT INTO `log`(`username`, `data`, `logmessage`) VALUES (?, CURDATE(), ?)"); 
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, logmessage);
        preparedStatement.executeUpdate();              
    }

}
