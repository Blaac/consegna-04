package com.jradar.model.devices.p4j;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

/**
 * The GPIOBox class.
 */
public class GPIOBox {

    private final Pin[] pinsDesc = {
            RaspiPin.GPIO_00, RaspiPin.GPIO_01, RaspiPin.GPIO_02, RaspiPin.GPIO_03,
            RaspiPin.GPIO_04, RaspiPin.GPIO_05, RaspiPin.GPIO_06, RaspiPin.GPIO_07,
            RaspiPin.GPIO_08, RaspiPin.GPIO_09, RaspiPin.GPIO_10, RaspiPin.GPIO_11,
            RaspiPin.GPIO_12, RaspiPin.GPIO_13, RaspiPin.GPIO_14, RaspiPin.GPIO_15,
            RaspiPin.GPIO_16, RaspiPin.GPIO_17, RaspiPin.GPIO_18, RaspiPin.GPIO_19,
            RaspiPin.GPIO_20 };

    private final GpioPinDigitalInput[] pins;

    /**
     * Constructor of the Class GPIOBox.
     */
    public GPIOBox() {
        final GpioController gpio = GpioFactory.getInstance();
        this.pins = new GpioPinDigitalInput[this.pinsDesc.length];
        for (int i = 0; i < this.pinsDesc.length; i++) {
            this.pins[i] = gpio.provisionDigitalInputPin(this.pinsDesc[i]);
        }
    }

    /**
     * Print the state.
     */
    public void printState() {
        for (int i = 0; i < this.pins.length; i++) {
            System.out.print(" " + i + ":" + this.pins[i].getState().getValue() + " ");
        }
        System.out.println(".");
    }
}
