package com.jradar.model.devices.p4j;

import com.jradar.model.devices.Button;
import com.jradar.model.observable.Observable;

/**
 * The ObservableButton class.
 */
public abstract class ObservableButton extends Observable implements Button { //NOPMD
}
