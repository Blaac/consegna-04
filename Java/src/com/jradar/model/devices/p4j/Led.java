package com.jradar.model.devices.p4j;

import java.io.IOException;

import com.jradar.model.devices.Light;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

/**
 * The Led class.
 */
@SuppressWarnings("unused")
public class Led implements Light {
    private final int pinNum; //NOPMD
    private GpioPinDigitalOutput pin;

    /**
     * Constructor of the Class Led.
     *
     * @param pinNum
     *            the pin number
     */
    public Led(final int pinNum) {

        this.pinNum = pinNum;
        try {
            final GpioController gpio = GpioFactory.getInstance();
            this.pin = gpio.provisionDigitalOutputPin(Config.PIN_MAP[pinNum]);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized boolean switchOn() throws IOException {
        this.pin.high();
        // System.out.println("LIGHT ON - pin "+pin);
        return false;
    }

    @Override
    public synchronized boolean switchOff() throws IOException {
        this.pin.low();
        // System.out.println("LIGHT OFF - pin "+pin);
        return false;
    }

}
