package com.jradar.model.devices.p4j;

import com.jradar.controller.event.Event;
import com.jradar.controller.event.EventMsg;
import com.jradar.controller.event.EventType;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

/**
 * The Button class.
 */
public class Button extends ObservableButton {

    private GpioPinDigitalInput pin;
    private final EventType eventType;
    private boolean flag = false;
    /**
     * Constructor of the Class Button.
     *
     * @param pinNum
     *            the pin num
     * @param eventType
     *            the {@link EventType}
     */
    public Button(final int pinNum, final EventType eventType) {
        super();
        this.eventType = eventType;
        try {
            final GpioController gpio = GpioFactory.getInstance();
            this.pin = gpio.provisionDigitalInputPin(Config.PIN_MAP[pinNum], PinPullResistance.PULL_DOWN);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        this.pin.addListener(new ButtonListener(this));
    }

    @Override
    public synchronized boolean isPressed() {
        return this.pin.isHigh();
    }

    class ButtonListener implements GpioPinListenerDigital {
        private final Button button;

        /**
         * Constructor of the Class ButtonListener.
         *
         * @param button
         *            the {@link Button}
         */
        ButtonListener(final Button button) {
            this.button = button;
        }

        @Override
        public void handleGpioPinDigitalStateChangeEvent(final GpioPinDigitalStateChangeEvent event) {
            Event ev = null;
            long time = 0 ;//= System.currentTimeMillis();
            
            System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
            if (event.getState().isHigh()) {
            	flag = true;
            	time = System.currentTimeMillis();
                //ev = new EventMsg(this.button.eventType);
            } else if (event.getState().isLow() && flag){
            	if (System.currentTimeMillis() - time >= 50) {
            		flag = false;
            		ev = new EventMsg(this.button.eventType);
            	}
            	
                //ev = new EventMsg(EventType.BUTTON_RELEASED);
            	this.button.notifyObservers(ev);
            }
        }
    }
}
