package com.jradar.model.devices;

/**
 * Light.
 *
 */
public interface Light extends Device {
    /**
     * Switch on.
     *
     * @return true if can switch on
     * @throws Exception
     *             the {@link Exception}
     */
    boolean switchOn() throws Exception; //NOPMD

    /**
     * Switch off.
     *
     * @return true if can switch off
     * @throws Exception
     *             the Exception
     */
    boolean switchOff() throws Exception; //NOPMD
}
