package com.jradar.model.observable;

import com.jradar.controller.event.Event;

/**
 * The Observer.
 *
 */
public interface Observer {

    /**
     * Get the notification.
     *
     * @param ev
     *            the {@link Event}.
     * @return true or false
     */
    boolean notifyObserver(Event ev);
}
