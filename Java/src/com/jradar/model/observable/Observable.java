package com.jradar.model.observable;

import java.util.LinkedList;
import java.util.List;

import com.jradar.controller.event.Event;

/**
 * The Observable.
 *
 */
public class Observable {

    private final List<Observer> observers;

    /**
     * Constructor.
     */
    protected Observable() {
        this.observers = new LinkedList<Observer>();
    }

    /**
     * Notify the event.
     *
     * @param ev
     *            the event
     */
    protected void notifyObservers(final Event ev) {
        synchronized (this.observers) {
            for (final Observer obs : this.observers) {
                obs.notifyObserver(ev);
            }
        }
    }

    /**
     * Add the observer.
     *
     * @param obs
     *            the observer
     */
    public void addObserver(final Observer obs) {
        synchronized (this.observers) {
            this.observers.add(obs);
        }
    }

    /**
     * Remove the observer.
     *
     * @param obs
     *            the observer to remove
     */
    public void removeObserver(final Observer obs) {
        synchronized (this.observers) {
            this.observers.remove(obs);
        }
    }

}
