package com.jradar.model.input;

/**
 * Serial.
 *
 */
public interface Input {

    /**
     * If there is a msg Avaiable.
     *
     * @return true if avaiable
     */
    boolean isMsgAvailable();

    /**
     * Read the message.
     *
     * @return the {@link String} message
     */
    String readMsg();

    /**
     * Send the message.
     *
     * @param msg
     *            the String message
     */
    void sendMsg(String msg);

    /**
     * Wait for the msg.
     *
     * @return the message
     * @throws InterruptedException
     *             the {@link InterruptedException}
     */
    String waitForMsg() throws InterruptedException;

    /**
     * Set the msg.
     * 
     * @param msg
     *            the msg
     */
    void setMsg(String msg);
}
