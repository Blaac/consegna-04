package com.jradar.model.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jssc.SerialPortList;

/**
 * Show the serial ports available on the PC.
 *
 */
public class SerialPorts {
    private final List<String> ports;

    /**
     * Constructor.
     */
    public SerialPorts() {
        this.ports = new ArrayList<>();
    }

    /**
     * Update the list of ports.
     *
     * @throws Exception
     *             the exception
     */
    public void updateListOfPorts() {
        this.ports.clear();
        Arrays.stream(SerialPortList.getPortNames()).forEach(t -> this.ports.add(t));
    }

    /**
     * Get the list of ports.
     *
     * @return the list of ports.
     */
    public List<String> getPorts() {
        return this.ports;
    }
}