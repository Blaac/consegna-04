package com.jradar.model.input;

import com.jradar.model.agents.ConsoleAgent;

/**
 * Emu serial. *
 *
 */
public final class InputHandler implements Input {
    private boolean msgAvailable;
    private String msg;
    private final ConsoleAgent console;

    /**
     * Constructor of the Class InputHandler.
     */
    public InputHandler() {
        this.console = new ConsoleAgent(this);
    }

    /**
     * Start the agent.
     */
    public void start() {
        this.console.start();
    }

    @Override
    public synchronized boolean isMsgAvailable() {
        return this.msgAvailable;
    }

    @Override
    public synchronized String readMsg() {
        final String m = this.msg;
        this.msgAvailable = false;
        this.msg = null;
        return m;
    }

    @Override
    public synchronized void sendMsg(final String msg) {
        System.out.println(msg);
    }

    /**
     * Set the msg.
     *
     * @param msg
     *            the String msg
     */
    @Override
    public synchronized void setMsg(final String msg) {
        this.msgAvailable = true;
        this.msg = msg;
        this.notifyAll();
    }

    @Override
    public synchronized String waitForMsg() throws InterruptedException {
        while (!this.msgAvailable) {
            this.wait();
        }
        final String m = this.msg;
        this.msgAvailable = false;
        this.msg = null;
        return m;
    }

}
