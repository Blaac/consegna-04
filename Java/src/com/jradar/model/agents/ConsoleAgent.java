package com.jradar.model.agents;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.jradar.model.input.Input;
import com.jradar.model.input.InputHandler;

/**
 *
 * The StdInputAgent class.
 */
public class ConsoleAgent extends Thread {
    private final Input inputAgent;

    /**
     * Constructor of the Class StdInputAgent.
     *
     * @param inputAgent
     *            the {@link InputHandler}
     */
    public ConsoleAgent(final Input inputAgent) {
        this.inputAgent = inputAgent;
    }

    @Override
    public void run() {
        //System.out.println("[StdInput Emu Agent] running.");
        final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                final String msg = input.readLine();
                this.inputAgent.setMsg(msg);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}