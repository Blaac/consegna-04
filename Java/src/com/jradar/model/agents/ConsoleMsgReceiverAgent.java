package com.jradar.model.agents;

import org.apache.log4j.Logger;

import com.jradar.controller.devicecontroller.DeviceController;
import com.jradar.controller.event.ConsoleCommands;
import com.jradar.model.input.Input;

/**
 * The {@link ConsoleMsgReceiverAgent}.
 */
public class ConsoleMsgReceiverAgent extends Thread {
    private static final Logger LOG = Logger.getLogger(ConsoleMsgReceiverAgent.class);

    private final Input serialDevice;
    private final DeviceController controller;

    /**
     * Constructor.
     *
     * @param serialDevice
     *            the {@link Input}
     * @param controller
     *            the {@link DeviceController}
     */
    public ConsoleMsgReceiverAgent(final Input serialDevice,
            final DeviceController controller) {
        this.serialDevice = serialDevice;
        this.controller = controller;
    }

    @Override
    public void run() {
        while (true) {
            try {
                final String msg = this.serialDevice.waitForMsg();
                //EXIT COMMAND
                if (msg.equals(ConsoleCommands.EXIT_COMM.getMessage())) {
                    System.exit(0);
                }
                //PORT COMMANDS
                if (msg.startsWith(ConsoleCommands.PORT_COMM.getMessage())) {
                    final String[] parsed = msg.split(":");
                    if (parsed.length < 2) {
                        continue;
                    }
                    //UPDATE
                    if (parsed[1].equals(ConsoleCommands.UPDATE_PORT_COMM.getMessage())) {
                        this.controller.updatePorts();
                        continue;
                    }
                    //CLOSE
                    if (parsed[1].equals(ConsoleCommands.CLOSE_PORT_COMM.getMessage())) {
                        this.controller.closePort();
                        continue;
                    }
                    int val = -1;
                    try {
                        val = Integer.parseInt(parsed[1]);
                    } catch (final NumberFormatException e) {
                        System.out.println("[" + parsed[1] + "]" + " IS NOT A NUMBER! ");
                        continue;
                    }

                    //PORT NUMBER
                    if ((val != 100) && ((val < this.controller.getPortNames().size()) && (val >= 0))) {
                        this.controller.openPort(this.controller.getPortNames().get(val));
                    } else {
                        LOG.warn("WRONG PORT: " + val);
                    }
                }
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
