package com.jradar.model.agents;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.jradar.controller.event.Event;
import com.jradar.controller.event.EventMsg;
import com.jradar.controller.event.EventType;
import com.jradar.model.observable.Observable;

/**
 * The {@link ObservableTimer}.
 *
 */
public class ObservableTimer extends Observable {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> tickHandle;
    private final Runnable tickTask;

    /**
     * Constructor.
     */
    public ObservableTimer() {

        this.tickTask = () -> {
            final Event ev = new EventMsg(EventType.TICK_MSG, (int) System.currentTimeMillis());
            this.notifyObservers(ev);
        };
    }

    /**
     * Start generating tick event.
     *
     * @param period
     *            period in milliseconds
     */
    public synchronized void start(final long period) {
        this.tickHandle = this.scheduler.scheduleAtFixedRate(this.tickTask, 0, period, TimeUnit.MILLISECONDS);
    }

    /**
     * Generate a tick event after a number of milliseconds.
     *
     * @param deltat
     *            the delta
     */
    public synchronized void scheduleTick(final long deltat) {
        this.scheduler.schedule(this.tickTask, deltat, TimeUnit.MILLISECONDS);
    }

    /**
     * Stop generating tick event. *
     */
    public synchronized void stop() {
        if (this.tickHandle != null) {
            this.tickHandle.cancel(false);
            this.tickHandle = null;
        }
    }

}
