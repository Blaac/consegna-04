package com.jradar.model.agents;

import org.apache.log4j.Logger;

import com.jradar.controller.event.EventMsg;
import com.jradar.controller.event.EventType;
import com.jradar.model.observable.Observable;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 * Simple Serial Monitor, adaptation from:
 *
 * http://playground.arduino.cc/Interfacing/Java.
 *
 */
public class SerialMonitorAgent extends Observable implements SerialPortEventListener {
    private static final Logger LOG = Logger.getLogger(SerialMonitorAgent.class);

    private SerialPort serialPort;
    private static final int RATE = 9600;

    private static final int MAX_LINE = 100;

    private StringBuffer stringBuffer = new StringBuffer(); //NOPMD
    private String[] parsedString;

    private static String rxData = "";
    private int cnt;

    private String portName;
    private int dataRate;

    /**
     * Constructor.
     *
     */
    public SerialMonitorAgent() {
        this(null, RATE);
    }

    /**
     * Constructor.
     *
     * @param portName
     *            the {@link String} port name
     * @param dataRate
     *            the data rate
     */
    public SerialMonitorAgent(final String portName, final int dataRate) {
        this.dataRate = dataRate;
        this.portName = portName;
    }

    /**
     * Start method.
     *
     */
    public void start() {
        System.out.println("Started\nPort: " + this.portName + "\nBauldRate: " + this.dataRate);

        try {
            this.serialPort = new SerialPort(this.portName);
            this.serialPort.openPort();
            this.serialPort.setParams(this.dataRate,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            final int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;
            this.serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN
                    | SerialPort.FLOWCONTROL_RTSCTS_OUT);
            // add event listeners
            this.serialPort.setEventsMask(mask);
            this.serialPort.addEventListener(this);
        } catch (final Exception e) {
            System.err.println(e.toString());
        }
    }

    /**
     * This should be called when you stop using the port. This will prevent port locking on platforms like Linux.
     *
     * @throws SerialPortException
     *             the {@link SerialPortException}
     */
    public synchronized void close() throws SerialPortException {
        if (this.serialPort != null) {
            this.serialPort.removeEventListener();
            this.serialPort.closePort();
            LOG.info("PORT CLOSED");
        }
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     */
    @Override
    public synchronized void serialEvent(final SerialPortEvent event) {
        if (event.isRXCHAR() && (event.getEventValue() > 0)) {
            try {
                String toParse = "";
                this.stringBuffer.append(this.serialPort.readString(event.getEventValue()));
                toParse = this.stringBuffer.toString();
                if (toParse.endsWith("\r\n")) {
                    this.parsedString = toParse.split("\n");
                    rxData = this.parsedString[this.cnt];
                    rxData = rxData.substring(0, rxData.length() - 1);
                    this.notifyObservers(new EventMsg(EventType.ARDUINO_SERIAL_MSG, rxData));
                    this.cnt++;
                    if (this.stringBuffer.length() > MAX_LINE) {
                        this.cnt = 0;
                        this.stringBuffer = new StringBuffer();
                    }
                }
            } catch (final SerialPortException e) {
                System.out.println("SerialEvent error:" + e.toString());
            }
        }
    }

    /**
     * Send data.
     *
     * @param data
     *            the String data
     * @throws Exception
     *             the Exception
     */
    public void sendData(final String data) {
        try {
            this.serialPort.writeString(data + "\n");
        } catch (final Exception e) {
            LOG.warn("MAN! SELECT PORT: " + e.getMessage());
        }
    }

    /**
     * Set the port name.
     *
     * @param portName
     *            the {@link String} port name
     */
    public void setPortName(final String portName) {
        this.portName = portName;
    }

    /**
     * Set the bauld rate.
     *
     * @param dataRate
     *            the bauld rate
     */
    public void setDataRate(final int dataRate) {
        this.dataRate = dataRate;
    }

    /**
     * Get the port name.
     *
     * @return the port name
     */
    public String getPortName() {
        return this.portName;
    }

    /**
     * Get the data rate.
     *
     * @return the data rate
     */
    public int getDataRate() {
        return this.dataRate;
    }

    /**
     * Check if the port is owned by somebody.
     *
     * @return true if is owned by somebody
     */
    public boolean isAlreadyOpened() {
        try {
            return this.serialPort.isOpened();
        } catch (final Exception e) {
            LOG.warn("SELECT A PORT MAN!");
            return false;
        }
    }

    /**
     * Get the {@link SerialPort}.
     *
     * @return true if can open serial port
     */
    public boolean canOpenSerialPort() {
        return this.serialPort == null;
    }

}