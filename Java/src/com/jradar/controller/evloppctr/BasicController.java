package com.jradar.controller.evloppctr;

/**
 * {@link BasicController}.
 *
 */
public abstract class BasicController extends Thread {
    /**
     * Wait for the seconds.
     *
     * @param ms
     *            the seconds
     * @throws InterruptedException
     *             the {@link InterruptedException}
     */
    protected void waitFor(final long ms) throws InterruptedException {
        Thread.sleep(ms);
    }
}
