package com.jradar.controller.evloppctr;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.jradar.controller.event.Event;
import com.jradar.model.observable.Observable;
import com.jradar.model.observable.Observer;

/**
 *
 *
 */
public abstract class BasicEventLoopController extends Thread implements Observer {

    private static final int DEF_EVENT_QUEUE_SIZE = 50;
    private final BlockingQueue<Event> eventQueue;

    /**
     * Constructor.
     *
     * @param size
     *            the size
     */
    protected BasicEventLoopController(final int size) {
        this.eventQueue = new ArrayBlockingQueue<Event>(size);
    }

    /**
     * Cosntructor 2.
     */
    protected BasicEventLoopController() {
        this(DEF_EVENT_QUEUE_SIZE);
    }

    /**
     * Process the event from the {@link BasicEventLoopController}.
     *
     * @param ev
     *            the {@link Event}.
     */
    protected abstract void processEvent(Event ev);

    @Override
    public void run() {
        while (true) {
            try {
                final Event ev = this.waitForNextEvent();
                this.processEvent(ev);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Start observing an {@link Observable}.
     *
     * @param object
     *            the {@link Observable}
     */
    protected void startObserving(final Observable object) {
        object.addObserver(this);
    }

    /**
     * Stop observing the {@link Observable}.
     *
     * @param object
     *            the {@link Observable}
     */
    protected void stopObserving(final Observable object) {
        object.removeObserver(this);
    }

    /**
     * Wait for the {@link Event} to be putted to the queue.
     *
     * @return the Event to process
     * @throws InterruptedException
     *             the {@link InterruptedException}
     */
    protected Event waitForNextEvent() throws InterruptedException {
        return this.eventQueue.take();
    }

    /**
     * Pick the event if avaiable.
     *
     * @return the Event
     * @throws InterruptedException
     *             the {@link InterruptedException}
     */
    protected Event pickNextEventIfAvail() throws InterruptedException {
        return this.eventQueue.poll();
    }

    /**
     * Get notified by the {@link Observable}.
     *
     * @param ev
     *            the {@link Event}
     * @return true if not empty
     */
    @Override
    public boolean notifyObserver(final Event ev) {
        return this.eventQueue.offer(ev);
    }
}
