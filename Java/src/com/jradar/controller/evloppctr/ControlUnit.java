package com.jradar.controller.evloppctr;

import org.apache.log4j.Logger;

import com.jradar.controller.event.Event;
import com.jradar.model.agents.ObservableTimer;
import com.jradar.model.devices.Door;
import com.jradar.model.devices.Light;
import com.jradar.model.devices.door.DoorImpl;

/**
 * Centralina.
 *
 */
@SuppressWarnings("unused")
public class ControlUnit extends BasicEventLoopController {
    private static final Logger LOG = Logger.getLogger(ControlUnit.class);

    private final Light ledInside;
    private final Light ledFailed; //NOPMD
    private final ObservableTimer timer; //NOPMD
    private final DoorImpl door;
    //private static final int TICK = 500;

    private enum State {
        IDLE, LOGGED;
    };

    private final State currentState;

    /**
     * Costruttore.
     *
     * @param ledInside
     *            the {@link Light}
     * @param ledFailed
     *            the Tracking led
     * @param door
     *            the {@link Door}
     *
     */
    public ControlUnit(final Light ledInside, final Light ledFailed,
            final DoorImpl door) {
        this.ledInside = ledInside;
        this.ledFailed = ledFailed;

        this.door = door;


        this.timer = new ObservableTimer();
        this.timer.addObserver(this);

        this.currentState = State.IDLE;

    }

    @Override
    protected void processEvent(final Event ev) {
        switch (this.currentState) { //NOPMD
        case IDLE:

            break;
        case LOGGED:
            break;
        default:
            break;
        }
    }

}
