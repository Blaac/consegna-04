package com.jradar.controller.devicecontroller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.jradar.model.agents.SerialMonitorAgent;
import com.jradar.model.input.SerialPorts;
import com.jradar.textui.UI;

import jssc.SerialPortException;

/**
 * Class that manages all the logic of the application.
 */
public final class DeviceController {
    private static final Logger LOG = Logger.getLogger(DeviceController.class);
    private final Set<UI> uis;
    private final SerialPorts ports;
    private final SerialMonitorAgent serialMonitor;

    /**
     * Constructor of the Class Controller.
     *
     * @param serialMonitor
     *            the {@link SerialMonitorAgent}
     *
     */
    public DeviceController(final SerialMonitorAgent serialMonitor) {
        this.uis = new HashSet<>();
        this.ports = new SerialPorts();
        this.serialMonitor = serialMonitor;
    }

    /**
     * Attache the ui.
     *
     * @param ui
     *            the {@link UI}
     */
    public void attacheUI(final UI ui) {
        this.uis.add(ui);
        this.uis.forEach(t -> {
            t.welcome();
            t.printPorts(this.getPortNames());
        });
    }

    /**
     * Update the port.
     */
    public void updatePorts() {
        this.ports.updateListOfPorts();
        this.uis.forEach(t -> t.printPorts(this.getPortNames()));
    }

    /**
     * Get all the ports avaiable.
     *
     * @return the list of port name
     */
    public List<String> getPortNames() {
        this.ports.updateListOfPorts();
        return this.ports.getPorts();
    }

    /**
     * Close the port.
     */
    public void closePort() {
        try {
            this.serialMonitor.close();
        } catch (final SerialPortException e) {
            LOG.error("ERROR CLOSING RXTX: " + e.getMessage());
        }
    }

    /**
     * Open the port.
     *
     * @param portName
     *            the {@link String} port name
     */
    public void openPort(final String portName) {
        if (!this.serialMonitor.isAlreadyOpened()) {
            this.serialMonitor.setPortName(portName);
            this.serialMonitor.start();
        } else {
            LOG.warn("MAN THE PORT IS ALREADY OPENED!");
        }
    }

    /**
     * Get data from RxTx.
     *
     * @param inputLine
     *            the Data*
     */
    public void updateFromRxTx(final String inputLine) {
        this.uis.forEach(t -> t.updateViewReceived(inputLine));
    }

}