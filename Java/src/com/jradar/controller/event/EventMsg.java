/**
 * The EventBaseMsg.java class.
 */
package com.jradar.controller.event;

import java.util.Optional;

/**
 * The EventBaseMsg class.
 */
public class EventMsg implements Event {
    private final EventType eventType;
    private final Optional<String> message;
    private final Optional<Integer> value;

    /**
     * Constructor of the Class EventBaseMsg.
     *
     * @param eventType
     *            the {@link EventType}
     * @param message
     *            the {@link String} message
     * @param value
     *            the value
     */
    public EventMsg(final EventType eventType, final String message, final Integer value) {
        super();
        this.eventType = eventType;
        this.message = Optional.ofNullable(message);
        this.value = Optional.ofNullable(value);
    }

    /**
     * Constructor of the Class EventMsg.
     *
     * @param eventType
     *            the {@link EventType}
     * @param message
     *            the {@link String} message
     */
    public EventMsg(final EventType eventType, final String message) {
        this(eventType, message, null);
    }

    /**
     * Constructor of the Class EventMsg.
     *
     * @param eventType
     *            the {@link EventType}
     * @param value
     *            the value
     */
    public EventMsg(final EventType eventType, final Integer value) {
        this(eventType, null, value);
    }

    /**
     * Constructor of the Class EventMsg.
     *
     * @param eventType
     *            the {@link EventType}
     */
    public EventMsg(final EventType eventType) {
        this(eventType, null, null);
    }

    /**
     * Get the eventType.
     *
     * @return the eventType
     */
    @Override
    public EventType getEventType() {
        return this.eventType;
    }

    @Override
    public Optional<String> getMessage() {
        return this.message;
    }

    @Override
    public Optional<Integer> getValue() {
        return this.value;
    }
}
