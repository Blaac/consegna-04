/**
 * The ConsoleCommands.java class.
 */
package com.jradar.controller.event;

/**
 * The ConsoleCommands class.
 */
public enum ConsoleCommands {
    /**
     * The EventType EXIT_MSG.
     */
    EXIT_COMM("exit"),
    /**
     * The port command.
     */
    PORT_COMM("port"),
    /**
     * The Port command.
     */
    UPDATE_PORT_COMM("update"),
    /**
     * Close port command.
     */
    CLOSE_PORT_COMM("close"),

    /**
     * Set Radar speed.
     */
    RADAR("radar");

    private final String command;

    /**
     * Constructor of the Class EventType.
     *
     * @param msg
     *            the {@link String} msg
     */
    ConsoleCommands(final String msg) {
        this.command = msg;
    }

    /**
     * Get the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.command;
    }

}
