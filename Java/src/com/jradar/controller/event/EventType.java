/**
 * The EventType.java class.
 */
package com.jradar.controller.event;

/**
 * The EventType class.
 */
public enum EventType {
    /**
     * The EventType SERIAL_MSG.
     */
    ARDUINO_SERIAL_MSG("serial"),
    /**
     * The EventType LOG_MSG.
     */
    LOG_MSG("9"),
    /**
     * The EventType LOGIN_MSG.
     */
    LOGIN_MSG("1"),
    /**
     * The EventType CLOSE.
     */
    CLOSE("8"),
    /**
     * The EventType ACCESS_GRANTED.
     */
    ACCESS_GRANTED("5true"),
    /**
     * The EventType ACCESS_DENIED.
     */
    ACCESS_DENIED("5false"),
    /**
     * The EventType PRESENCE_DETECTED.
     */
    PRESENCE_DETECTED("4userinside"),
    /**
     * The EventType PRESENCE_NOT_DETECTED.
     */
    PRESENCE_NOT_DETECTED("4failopen"),
    /**
     * The EventType TICK_MSG.
     */
    TICK_MSG("Tick"),
    /**
    * The EventType BUTTON_RELEASED.
    */
    BUTTON_RELEASED("button-released");

    private final String message;

    /**
     * Constructor of the Class EventType.
     *
     * @param msg
     *            the {@link String} msg
     */
    EventType(final String msg) {
        this.message = msg;
    }

    /**
     * Get the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

}
