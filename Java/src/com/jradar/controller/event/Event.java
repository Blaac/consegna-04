package com.jradar.controller.event;

import java.util.Optional;

/**
 * The Event.
 *
 */
public interface Event {

    /**
     * Get the {@link EventType}.
     *
     * @return the {@link EventType}
     */
    EventType getEventType();

    /**
     * Get the message.
     *
     * @return the message
     */
    Optional<String> getMessage();

    /**
     * Get the value.
     *
     * @return the value
     */
    Optional<Integer> getValue();
}
