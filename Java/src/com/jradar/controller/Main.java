package com.jradar.controller;

import java.io.IOException;

import com.jradar.controller.devicecontroller.DeviceController;
import com.jradar.controller.evloppctr.ControlUnit;
import com.jradar.model.agents.ConsoleMsgReceiverAgent;
import com.jradar.model.agents.SerialMonitorAgent;
import com.jradar.model.devices.Light;
import com.jradar.model.devices.door.DoorImpl;
import com.jradar.model.devices.emu.Led;
//import com.jradar.model.devices.p4j.Led;
import com.jradar.model.input.InputHandler;
import com.jradar.textui.ConsoleView;
import com.jradar.textui.UI;

/**
 * Class main.
 *
 */

public final class Main {

    private Main() {
    }

    /**
     * Main method.
     *
     * @param args
     *            the args
     * @throws IOException
     *             The exception
     */
    public static void main(final String[] args) throws IOException {
        //IO AGENTS
        final SerialMonitorAgent serialAgent = new SerialMonitorAgent();
        final InputHandler consoleHandler = new InputHandler();
//        final Light ledFailed = new Led(9);
//        final Light ledInside = new Led(7);
        final Light ledFailed= new com.jradar.model.devices.p4j.Led(9);
        final Light ledInside = new com.jradar.model.devices.p4j.Led(7);

        final DoorImpl door = new DoorImpl(serialAgent, ledFailed, ledInside);

     
        //IO CONTROLLER
        final DeviceController controller = new DeviceController(serialAgent);
        final ConsoleMsgReceiverAgent rec = new ConsoleMsgReceiverAgent(consoleHandler, controller);

        //GUI
        final UI ui = new ConsoleView();
        controller.attacheUI(ui);
        //START ALL AGENTS
        consoleHandler.start();
        rec.start();


        final Thread shut = new Thread(() -> {
            try {
                ledFailed.switchOff();
                ledInside.switchOff();
            } catch (final Exception e) {
                e.printStackTrace();
            }
        });
        Runtime.getRuntime().addShutdownHook(shut);
    }
}
